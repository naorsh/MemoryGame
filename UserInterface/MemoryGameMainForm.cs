﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using System.Threading;
using MemoryGameLogic;

namespace UserInterface
{
    public class MemoryGameMainForm : Form
    {
        private MemoryGameSettingsForm m_MemoryGameSettingsForm;
        private Game m_MemoryGame;
        private CardButton[,] m_CardButtonMatrix = null;
        private Player m_CurrentPlayer;
        private Player m_NextPlayer;
        private Label currentPlayerLabel = new Label();
        private Label firstPlayerScoreLabel = new Label();
        private Label secondPlayerScoreLabel = new Label();
        private int m_BoardRows;
        private int m_BoardCols;
        private int m_paddingSize;
        private CardButton m_FirstSelectedCard;
        private CardButton m_SecondSelectedCard;
        private int m_ButtonSizeX = 70;
        private int m_ButtonSizeY = 70;

        public MemoryGameMainForm()
        {
            this.Text = "Memory Game";
            m_MemoryGameSettingsForm = new MemoryGameSettingsForm();
            if (m_MemoryGameSettingsForm.ShowDialog() == DialogResult.OK)
            {
                m_BoardRows = m_MemoryGameSettingsForm.BoardRowsChoice;
                m_BoardCols = m_MemoryGameSettingsForm.BoardColsChoice;
                m_MemoryGame = new Game();
                m_MemoryGame.CreateNewBoard(m_BoardRows, m_BoardCols);
                m_MemoryGame.CreatePlayer1(m_MemoryGameSettingsForm.FirstPlayerName, false, true);
                m_MemoryGame.CreatePlayer2(m_MemoryGameSettingsForm.SecondPlayerName, m_MemoryGameSettingsForm.IsSecondPlayerComputer, false);
                m_CurrentPlayer = m_MemoryGame.Player1;
                m_NextPlayer = m_MemoryGame.Player2;
                m_CardButtonMatrix = new CardButton[m_BoardRows, m_BoardCols];
                this.AutoSize = true;
                m_paddingSize = 15;
                this.Padding = new Padding(m_paddingSize);
                this.FormBorderStyle = FormBorderStyle.FixedSingle;
                this.MaximizeBox = false;
            }
            else
            {
                Environment.Exit(0);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            initControls();
        }

        private void initControls()
        {
            drawBoard();
            drawPlayersInfo();
        }

        private void drawBoard()
        {
            int currentLocationX = m_paddingSize;
            int currentLocationY = m_paddingSize;

            for (int i = 0; i < m_BoardRows; i++)
            {
                for (int j = 0; j < m_BoardCols; j++)
                {
                    m_CardButtonMatrix[i, j] = new CardButton();
                    m_CardButtonMatrix[i, j].TabStop = false;
                    m_CardButtonMatrix[i, j].X = i;
                    m_CardButtonMatrix[i, j].Y = j;
                    m_CardButtonMatrix[i, j].Text = string.Empty;
                    m_CardButtonMatrix[i, j].Location = new Point(currentLocationX, currentLocationY);
                    m_CardButtonMatrix[i, j].Size = new Size(m_ButtonSizeX, m_ButtonSizeY);
                    currentLocationX += m_ButtonSizeX + 5;
                    m_CardButtonMatrix[i, j].Click += cardButton_FirstClick;
                    Controls.Add(m_CardButtonMatrix[i, j]);
                }

                currentLocationX = m_paddingSize;
                currentLocationY += m_ButtonSizeY + 5;
            }
        }

        private void drawPlayersInfo()
        {
            int currentLabelLocation = ((m_ButtonSizeY + 5) * m_BoardRows) + (m_paddingSize * 2);
            currentPlayerLabel.Location = new Point(m_paddingSize, currentLabelLocation);
            currentPlayerLabel.BackColor = Color.Aquamarine;
            currentPlayerLabel.AutoSize = true;
            currentPlayerLabel.Text = string.Format("Current Player: {0}", m_CurrentPlayer.Name);
            Controls.Add(currentPlayerLabel);
            currentLabelLocation += m_paddingSize * 2;

            firstPlayerScoreLabel.AutoSize = true;
            firstPlayerScoreLabel.Text = string.Format("{0}: {1} Pairs", m_MemoryGame.Player1.Name, m_MemoryGame.Player1.Score);
            firstPlayerScoreLabel.Location = new Point(m_paddingSize, currentLabelLocation);
            firstPlayerScoreLabel.BackColor = Color.Aquamarine;
            Controls.Add(firstPlayerScoreLabel);
            currentLabelLocation += m_paddingSize * 2;

            secondPlayerScoreLabel.AutoSize = true;
            secondPlayerScoreLabel.Text = string.Format("{0}: {1} Pairs", m_MemoryGame.Player2.Name, m_MemoryGame.Player2.Score);
            secondPlayerScoreLabel.Location = new Point(m_paddingSize, currentLabelLocation);
            secondPlayerScoreLabel.BackColor = Color.MediumOrchid;
            Controls.Add(secondPlayerScoreLabel);
        }

        private void cardButton_FirstClick(object sender, EventArgs e)
        {
            m_FirstSelectedCard = sender as CardButton;
            m_MemoryGame.flipAndReturnCard(m_FirstSelectedCard.X, m_FirstSelectedCard.Y);
            m_FirstSelectedCard.Text = m_MemoryGame.Board.BoardArray[m_FirstSelectedCard.X, m_FirstSelectedCard.Y].LetterValue.ToString();
            if (m_CurrentPlayer == m_MemoryGame.Player1)
            {
                m_FirstSelectedCard.BackColor = Color.Aquamarine;
            }
            else
            {
                m_FirstSelectedCard.BackColor = Color.MediumOrchid;
            }

            m_FirstSelectedCard.Enabled = false;

            foreach (CardButton cardButton in m_CardButtonMatrix)
            {
                cardButton.Click -= this.cardButton_FirstClick;
                cardButton.Click += this.cardButton_SecondClick;
            }
        }

        private void cardButton_SecondClick(object sender, EventArgs e)
        {
            bool cardsCompare = false;
            m_SecondSelectedCard = sender as CardButton;

            m_MemoryGame.flipAndReturnCard(m_SecondSelectedCard.X, m_SecondSelectedCard.Y);
            m_SecondSelectedCard.Text = m_MemoryGame.Board.BoardArray[m_SecondSelectedCard.X, m_SecondSelectedCard.Y].LetterValue.ToString();

            if (m_CurrentPlayer == m_MemoryGame.Player1)
            {
                m_SecondSelectedCard.BackColor = Color.Aquamarine;
            }
            else
            {
                m_SecondSelectedCard.BackColor = Color.MediumOrchid;
            }

            m_SecondSelectedCard.Enabled = false;
            cardsCompare = m_MemoryGame.CompareSelectedCards(m_CurrentPlayer, m_NextPlayer, m_MemoryGame.Board.BoardArray[m_FirstSelectedCard.X, m_FirstSelectedCard.Y], m_MemoryGame.Board.BoardArray[m_SecondSelectedCard.X, m_SecondSelectedCard.Y]);

            foreach (CardButton cardButton in m_CardButtonMatrix)
            {
                cardButton.Click -= this.cardButton_SecondClick;
                cardButton.Click += this.cardButton_FirstClick;
            }

            if (cardsCompare)
            {
                firstPlayerScoreLabel.Text = string.Format("{0}: {1} Pairs", m_MemoryGame.Player1.Name, m_MemoryGame.Player1.Score);
                secondPlayerScoreLabel.Text = string.Format("{0}: {1} Pairs", m_MemoryGame.Player2.Name, m_MemoryGame.Player2.Score);
            }
            else
            {
                Thread.Sleep(1000);
                m_FirstSelectedCard.Enabled = true;
                m_FirstSelectedCard.Text = string.Empty;
                m_FirstSelectedCard.BackColor = Color.Empty;             
                m_SecondSelectedCard.Enabled = true;
                m_SecondSelectedCard.Text = string.Empty;
                m_SecondSelectedCard.BackColor = Color.Empty;

                updateCurrentPlayer();
                currentPlayerLabel.Text = string.Format("Current Player: {0}", m_CurrentPlayer.Name);
                currentPlayerLabel.Refresh();
            }

            if (!cardsCompare && m_CurrentPlayer.IsComputer)
            {
                playComputerTurn();
                currentPlayerLabel.Text = string.Format("Current Player: {0}", m_CurrentPlayer.Name);
                updateCurrentPlayer();
            }

            if (m_MemoryGame.checkIfBoardIsFull())
            {
                askUserForAnotherGame();
            }
        }

        private void playComputerTurn()
        {
            Point firstCard, secondCard;
            bool cardsCompare = false;

            foreach (CardButton cardButton in m_CardButtonMatrix)
            {
                cardButton.Enabled = false;
            }

            do
            {  
                firstCard = m_MemoryGame.ChooseRandomPointOnBoard();
                m_MemoryGame.flipAndReturnCard(firstCard.X, firstCard.Y);
                m_CardButtonMatrix[firstCard.X, firstCard.Y].Text = m_MemoryGame.Board.BoardArray[firstCard.X, firstCard.Y].LetterValue.ToString();
                m_CardButtonMatrix[firstCard.X, firstCard.Y].BackColor = Color.MediumOrchid;
                m_CardButtonMatrix[firstCard.X, firstCard.Y].Refresh();
                Thread.Sleep(1000);
                secondCard = m_MemoryGame.ChooseRandomPointOnBoard();
                m_MemoryGame.flipAndReturnCard(secondCard.X, secondCard.Y);
                m_CardButtonMatrix[secondCard.X, secondCard.Y].Text = m_MemoryGame.Board.BoardArray[secondCard.X, secondCard.Y].LetterValue.ToString();
                m_CardButtonMatrix[secondCard.X, secondCard.Y].BackColor = Color.MediumOrchid;
                m_CardButtonMatrix[secondCard.X, secondCard.Y].Refresh();
                Thread.Sleep(1000);
                cardsCompare = m_MemoryGame.CompareSelectedCards(m_CurrentPlayer, m_NextPlayer, m_MemoryGame.Board.BoardArray[firstCard.X, firstCard.Y], m_MemoryGame.Board.BoardArray[secondCard.X, secondCard.Y]);
                if (!cardsCompare)
                {
                    m_CardButtonMatrix[firstCard.X, firstCard.Y].Text = string.Empty;
                    m_CardButtonMatrix[firstCard.X, firstCard.Y].BackColor = Color.Empty;
                    m_CardButtonMatrix[secondCard.X, secondCard.Y].Text = string.Empty;
                    m_CardButtonMatrix[secondCard.X, secondCard.Y].BackColor = Color.Empty;
                }
                else
                {
                    m_CardButtonMatrix[firstCard.X, firstCard.Y].Enabled = false;
                    m_CardButtonMatrix[secondCard.X, secondCard.Y].Enabled = false;
                    currentPlayerLabel.Text = string.Format("Current Player: {0}", m_CurrentPlayer.Name);
                    firstPlayerScoreLabel.Text = string.Format("{0}: {1} Pairs", m_MemoryGame.Player1.Name, m_MemoryGame.Player1.Score);
                    secondPlayerScoreLabel.Text = string.Format("{0}: {1} Pairs", m_MemoryGame.Player2.Name, m_MemoryGame.Player2.Score);
                }

                if (m_MemoryGame.checkIfBoardIsFull())
                {
                    askUserForAnotherGame();
                    break;
                }
            }
            while (cardsCompare);

            foreach (CardButton cardButton in m_CardButtonMatrix)
            {
                if (m_MemoryGame.Board.BoardArray[cardButton.X, cardButton.Y].IsHidden)
                {
                    cardButton.Enabled = true;
                }
            }

            updateCurrentPlayer();
        }

        private void updateCurrentPlayer()
        {
            if (m_MemoryGame.Player1.IsPlayerTurn)
            {
                m_CurrentPlayer = m_MemoryGame.Player1;
                m_NextPlayer = m_MemoryGame.Player2;
                currentPlayerLabel.BackColor = Color.Aquamarine;
            }
            else
            {
                m_CurrentPlayer = m_MemoryGame.Player2;
                m_NextPlayer = m_MemoryGame.Player1;
                currentPlayerLabel.BackColor = Color.MediumOrchid;
            }
        }

        private void askUserForAnotherGame()
        {
            string winnerMessage = string.Empty;
            if (m_MemoryGame.Player1.Score > m_MemoryGame.Player2.Score)
            {
                winnerMessage = string.Format("{0} Won! {1} Score: {2} {3}", m_MemoryGame.Player1.Name, Environment.NewLine, m_MemoryGame.Player1.Score, Environment.NewLine);
            }
            else if (m_MemoryGame.Player1.Score < m_MemoryGame.Player2.Score)
            {
                winnerMessage = string.Format("{0} Won! {1} Score: {2} {3}", m_MemoryGame.Player2.Name, Environment.NewLine, m_MemoryGame.Player2.Score, Environment.NewLine);
            }
            else
            {
                winnerMessage = string.Format("It's a tie!{0}", Environment.NewLine);
            }

            DialogResult askUserForAnotherGameMessage = MessageBox.Show(string.Format("{0} Would you like a rematch?", winnerMessage), "End Game!", MessageBoxButtons.YesNo);
            if (askUserForAnotherGameMessage == DialogResult.Yes)
            {
                m_MemoryGame = new Game();
                m_MemoryGame.CreateNewBoard(m_BoardRows, m_BoardCols);
                m_MemoryGame.CreatePlayer1(m_MemoryGameSettingsForm.FirstPlayerName, false, true);
                m_MemoryGame.CreatePlayer2(m_MemoryGameSettingsForm.SecondPlayerName, m_MemoryGameSettingsForm.IsSecondPlayerComputer, false);
                m_CurrentPlayer = m_MemoryGame.Player1;
                m_NextPlayer = m_MemoryGame.Player2;

                foreach (CardButton cardButton in m_CardButtonMatrix)
                {
                    cardButton.BackColor = Color.Empty;
                    cardButton.Text = string.Empty;
                    cardButton.Enabled = true;
                }

                currentPlayerLabel.BackColor = Color.Aquamarine;
                currentPlayerLabel.Text = string.Format("Current Player: {0}", m_CurrentPlayer.Name);
                currentPlayerLabel.Refresh();
                firstPlayerScoreLabel.Text = string.Format("{0}: {1} Pairs", m_MemoryGame.Player1.Name, m_MemoryGame.Player1.Score);
                secondPlayerScoreLabel.Text = string.Format("{0}: {1} Pairs", m_MemoryGame.Player2.Name, m_MemoryGame.Player2.Score);
            }
            else
            {
                System.Environment.Exit(0);
            }
        }
    }
}
