﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace UserInterface
{
    public class CardButton : Button
    {
        private int m_X = 0, m_Y = 0;

        public int X
        {
            get { return m_X; }
            set { m_X = value; }
        }

        public int Y
        {
            get { return m_Y; }
            set { m_Y = value; }
        }
    }
}
