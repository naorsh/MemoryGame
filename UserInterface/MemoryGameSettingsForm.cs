﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UserInterface
{
    public class MemoryGameSettingsForm : Form
    {
        private Button startButton = new Button();
        private Button chooseSizeButton = new Button();
        private Button chooseSecondPlayerTypeButton = new Button();
        private int m_MaxLengthOfBoard;
        private int m_MinLengthOfBoard;
        private int m_BoardRowsChoice;
        private int m_BoardColsChoice;
        private string m_FirstPlayerName = string.Empty;
        private string m_SecondPlayerName = string.Empty;
        private bool m_isSecondPlayerComputer;
        private Label firstPlayerNameLabel = new Label();
        private Label secondPlayerNameLabel = new Label();
        private Label boardSizeLabel = new Label();
        private TextBox firstPlayerNameTextBox = new TextBox();
        private TextBox secondPlayerNameTextBox = new TextBox();

        public MemoryGameSettingsForm()
        {
            m_MaxLengthOfBoard = MemoryGameLogic.Game.sr_MaxLengthOfBoard;
            m_MinLengthOfBoard = MemoryGameLogic.Game.sr_MinLengthOfBoard;
            m_BoardRowsChoice = m_MinLengthOfBoard;
            m_BoardColsChoice = m_MinLengthOfBoard;
            m_isSecondPlayerComputer = true;

            this.Text = "MemoryGame - Settings";
            this.ClientSize = new Size(375, 179);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        public bool IsSecondPlayerComputer
        {
            get { return m_isSecondPlayerComputer; }
        }

        public string FirstPlayerName
        {
            get { return m_FirstPlayerName; }
        }

        public string SecondPlayerName
        {
            get { return m_SecondPlayerName; }
        }

        public int BoardRowsChoice
        {
            get { return m_BoardRowsChoice; }
        }

        public int BoardColsChoice
        {
            get { return m_BoardColsChoice; }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            initControls();
        }

        private void initControls()
        {
            firstPlayerNameLabel.AutoSize = true;
            firstPlayerNameLabel.Text = "First Player Name:";
            firstPlayerNameLabel.Location = new Point(21, 19);
            Controls.Add(firstPlayerNameLabel);
            secondPlayerNameLabel.AutoSize = true;
            secondPlayerNameLabel.Text = "Second Player Name:";
            secondPlayerNameLabel.Location = new Point(21, 45);
            Controls.Add(secondPlayerNameLabel);
            boardSizeLabel.AutoSize = true;
            boardSizeLabel.Text = "Board Size:";
            boardSizeLabel.Location = new Point(21, 77);
            Controls.Add(boardSizeLabel);
            firstPlayerNameTextBox.AutoSize = true;
            firstPlayerNameTextBox.Location = new Point(146, 17);
            firstPlayerNameTextBox.MaxLength = 15;
            Controls.Add(firstPlayerNameTextBox);
            secondPlayerNameTextBox.AutoSize = true;
            secondPlayerNameTextBox.Location = new Point(146, 44);
            secondPlayerNameTextBox.Enabled = false;
            secondPlayerNameTextBox.Text = "- computer -";
            secondPlayerNameTextBox.MaxLength = 15;
            Controls.Add(secondPlayerNameTextBox);
            chooseSecondPlayerTypeButton.AutoSize = true;
            chooseSecondPlayerTypeButton.Text = "Against a Friend";
            chooseSecondPlayerTypeButton.Location = new Point(252, 42);
            chooseSecondPlayerTypeButton.Click += this.chooseSecondPlayerTypeButton_ClickForPlayer;
            Controls.Add(chooseSecondPlayerTypeButton);
            chooseSizeButton.BackColor = Color.FromArgb(192, 192, 255);
            chooseSizeButton.Text = string.Format("{0} x {1}", m_BoardRowsChoice, m_BoardColsChoice);
            chooseSizeButton.Location = new Point(24, 97);
            chooseSizeButton.Size = new Size(107, 64);
            chooseSizeButton.Click += this.chooseSizeButton_Click;
            Controls.Add(chooseSizeButton);
            startButton.BackColor = Color.FromArgb(0, 192, 0);
            startButton.Text = "Start!";
            startButton.Location = new Point(274, 138);
            startButton.Size = new Size(75, 23);
            startButton.Click += this.startButton_Click;
            Controls.Add(startButton);
        }

        private void chooseSecondPlayerTypeButton_ClickForPlayer(object sender, EventArgs e)
        {
            secondPlayerNameTextBox.Enabled = true;
            secondPlayerNameTextBox.Text = string.Empty;
            chooseSecondPlayerTypeButton.Text = "Against Computer";
            m_isSecondPlayerComputer = false;
            chooseSecondPlayerTypeButton.Click -= chooseSecondPlayerTypeButton_ClickForPlayer;
            chooseSecondPlayerTypeButton.Click += this.chooseSecondPlayerTypeButton_ClickForComputer;
        }

        private void chooseSecondPlayerTypeButton_ClickForComputer(object sender, EventArgs e)
        {
            secondPlayerNameTextBox.Enabled = false;
            secondPlayerNameTextBox.Text = "- computer -";
            chooseSecondPlayerTypeButton.Text = "Against a Friend";
            m_isSecondPlayerComputer = true;
            chooseSecondPlayerTypeButton.Click -= chooseSecondPlayerTypeButton_ClickForComputer;
            chooseSecondPlayerTypeButton.Click += this.chooseSecondPlayerTypeButton_ClickForPlayer;
        }

        private void chooseSizeButton_Click(object sender, EventArgs e)
        {
            m_BoardColsChoice++;
            if ((m_BoardColsChoice * m_BoardRowsChoice) % 2 != 0)
            {
                m_BoardColsChoice++;
            }

            if (m_BoardColsChoice > m_MaxLengthOfBoard)
            {
                m_BoardColsChoice = m_MinLengthOfBoard;
                m_BoardRowsChoice++;
            }

            if (m_BoardRowsChoice > m_MaxLengthOfBoard)
            {
                m_BoardRowsChoice = m_MinLengthOfBoard;
            }

            chooseSizeButton.Text = string.Format("{0} x {1}", m_BoardRowsChoice, m_BoardColsChoice);
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            if (firstPlayerNameTextBox.Text != string.Empty && secondPlayerNameTextBox.Text != string.Empty)
            {
                m_FirstPlayerName = firstPlayerNameTextBox.Text;
                m_SecondPlayerName = secondPlayerNameTextBox.Text;
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("You can't leave empty names!", "Error");
            }
        }
    }
}
