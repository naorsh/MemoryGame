﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MemoryGameLogic
{
    public class Board
    {
        private int m_Rows = 0;
        private int m_Cols = 0;
        private Card[,] m_BoardArray = null;
        private int m_NumberOfCards = 0;

        public Board(int i_Rows = 0, int i_Cols = 0)
        {
            m_Rows = i_Rows;
            m_Cols = i_Cols;
            m_NumberOfCards = m_Rows * m_Cols;
            m_BoardArray = new Card[m_Rows, m_Cols];
        }

        public int Rows
        {
            get { return m_Rows; }
            set
            {
                m_Rows = value;
            }
        }

        public int Cols
        {
            get { return m_Cols; }
            set
            {
                m_Cols = value;
            }
        }

        public int NumberOfCards
        {
            get { return m_NumberOfCards; }
            set
            {
                m_NumberOfCards = value;
            }
        }

        public Card[,] BoardArray
        {
            get { return m_BoardArray; }
            set
            {
                m_BoardArray = value;
            }
        }

        public void FillNewBoard()
        {
            Random randomNumber = new Random();
            int randomLocation = 0;
            List<char> cardsValueArray = new List<char>();
            for(int i = 0; i < m_NumberOfCards / 2; i++)
            {
                cardsValueArray.Add((char)('A' + i));
                cardsValueArray.Add((char)('A' + i));
            }

            for (int i = 0; i < m_Rows; i++)
            {
                for (int j = 0; j < m_Cols; j++)
                {
                    randomLocation = randomNumber.Next(0, cardsValueArray.Count - 1);
                    m_BoardArray[i, j] = new Card(cardsValueArray[randomLocation], true);
                    cardsValueArray.RemoveAt(randomLocation);
                }
            }
        }

        public StringBuilder createBoardSketch()
        {
            StringBuilder boardSketch = new StringBuilder("  ");
            StringBuilder rowDevider = new StringBuilder("   =");
            char cardToShow = ' ';

            for (int i = 0; i < m_Cols; i++)
            {
                boardSketch.AppendFormat("   {0}", (char)('A' + i));
                rowDevider.Append("====");
            }

            boardSketch.Append("\n");
            boardSketch.AppendLine(rowDevider.ToString());

            for (int i = 0; i < m_Rows; i++)
            {
                boardSketch.AppendFormat("{0}  | ", i + 1);

                for (int j = 0; j < m_Cols; j++)
                {
                    cardToShow = m_BoardArray[i, j].IsHidden ? ' ' : m_BoardArray[i, j].LetterValue;
                    boardSketch.AppendFormat("{0} | ", cardToShow);
                }

                boardSketch.Append("\n");
                boardSketch.AppendLine(rowDevider.ToString());
            }

            return boardSketch;
        }
    }
}
