﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace MemoryGameLogic
{
    public class Game
    {
        public static readonly int sr_MaxLengthOfBoard = 6;
        public static readonly int sr_MinLengthOfBoard = 4;
        private Board m_Board = null;
        private Player m_Player1 = null, m_Player2 = null;
        private int m_NumberOfFlippedCards = 0;

        public Player Player1
        {
            get { return m_Player1; }
            set
            {
                m_Player1 = value;
            }
        }

        public int NumberOfFlippedCards
        {
            get { return m_NumberOfFlippedCards; }
            set
            {
                m_NumberOfFlippedCards = value;
            }
        }

        public Player Player2
        {
            get { return m_Player2; }
            set
            {
                m_Player2 = value;
            }
        }

        public Board Board
        {
            get { return m_Board; }
            set
            {
                m_Board = value;
            }
        }

        private bool isLengthCorrect(int i_Length)
        {
            if ((i_Length >= sr_MinLengthOfBoard) && (i_Length <= sr_MaxLengthOfBoard))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsCardPositionCorrect(int i_CardPositionRow, int i_CardPositionCol)
        {
            if(i_CardPositionRow < Board.Rows && i_CardPositionRow >= 0 && i_CardPositionCol < Board.Cols && i_CardPositionCol >= 0)
            {
                if (Board.BoardArray[i_CardPositionRow, i_CardPositionCol].IsHidden)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool CreateNewBoard(int i_Rows, int i_Cols)
        {
            if(isLengthCorrect(i_Rows) && isLengthCorrect(i_Cols) && (i_Rows * i_Cols) % 2 == 0)
            {
                m_Board = new Board(i_Rows, i_Cols);
                m_Board.FillNewBoard();
                return true;
            }
            else
            {
                return false;
            }
        }

        public void CreatePlayer1(string i_Name, bool i_IsComputer, bool i_IsPlayerTurn)
        {
            m_Player1 = new Player(i_Name, i_IsComputer, i_IsPlayerTurn);
        }

        public void CreatePlayer2(string i_Name, bool i_IsComputer, bool i_IsPlayerTurn)
        {
            m_Player2 = new Player(i_Name, i_IsComputer, i_IsPlayerTurn);
        }

        public Card flipAndReturnCard(int i_RowLocation, int i_ColLocation)
        {
            Card selectedCard = m_Board.BoardArray[i_RowLocation, i_ColLocation];
            selectedCard.IsHidden = false;
            return selectedCard;
        }

        public bool CompareSelectedCards(Player i_CurrentPlayer, Player i_NextPlayer, Card i_FirstCard, Card i_SecondCard)
        {
            if(i_FirstCard.LetterValue == i_SecondCard.LetterValue)
            {
                m_NumberOfFlippedCards += 2;
                i_CurrentPlayer.Score++;
                return true;
            }
            else
            {
                i_FirstCard.IsHidden = true;
                i_SecondCard.IsHidden = true;
                i_CurrentPlayer.IsPlayerTurn = false;
                i_NextPlayer.IsPlayerTurn = true;
                return false;
            }
        }

        public bool checkIfBoardIsFull()
        {
            if(m_NumberOfFlippedCards == Board.NumberOfCards)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void PlayComputerTurn()
        {
            Card firstCard = null;
            Card secondCard = null;
            do
            {
                firstCard = ChooseAndFlipRandomCard();
                secondCard = ChooseAndFlipRandomCard();
            }
            while (CompareSelectedCards(m_Player2, m_Player1, firstCard, secondCard));
        }

        public Card ChooseAndFlipRandomCard()
        {
            Point cardLocation = ChooseRandomPointOnBoard();
            return flipAndReturnCard(cardLocation.X, cardLocation.Y);
        }

        public Point ChooseRandomPointOnBoard()
        {
            Random randomNumber = new Random();
            int randomRowLocation = 0;
            int randomColLocation = 0;

            do
            {
                randomRowLocation = randomNumber.Next(0, m_Board.Rows);
                randomColLocation = randomNumber.Next(0, m_Board.Cols);
            }
            while (!m_Board.BoardArray[randomRowLocation, randomColLocation].IsHidden);
            return new Point(randomRowLocation, randomColLocation);
        }
    }
}
