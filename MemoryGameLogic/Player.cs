﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MemoryGameLogic
{
    public class Player
    {
        private string m_Name = string.Empty;
        private int m_Score = 0;
        private bool m_IsComputer = false;
        private bool m_IsPlayerTurn = false;

        public Player(string i_Name, bool i_IsComupter, bool i_IsPlayerTurn)
        {
            m_Name = i_Name;
            m_IsComputer = i_IsComupter;
            m_IsPlayerTurn = i_IsPlayerTurn;
            m_Score = 0;
        }

        public string Name
        {
            get { return m_Name; }
            set
            {
                m_Name = value;
            }
        }

        public int Score
        {
            get { return m_Score; }
            set
            {
                m_Score = value;
            }
        }

        public bool IsComputer
        {
            get { return m_IsComputer; }
            set
            {
                m_IsComputer = value;
            }
        }

        public bool IsPlayerTurn
        { 
            get { return m_IsPlayerTurn; }
            set
            {
                m_IsPlayerTurn = value;
            }
        }
    }
}
