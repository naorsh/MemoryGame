﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MemoryGameLogic
{
    public class Card
    {
        private char m_LetterValue = '0';
        private bool m_IsHidden = true;

        public Card(char i_Value, bool i_IsHidden)
        {
            this.m_LetterValue = i_Value;
            this.m_IsHidden = i_IsHidden;
        }

        public char LetterValue
        {
            get { return m_LetterValue; }
            set
            {
                m_LetterValue = value;
            }
        }

        public bool IsHidden
        {
            get { return m_IsHidden; }
            set
            {
                m_IsHidden = value;
            }
        }
    }
}
